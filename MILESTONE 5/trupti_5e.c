// returns the position where str2 is in str1 and if not present returns -1.


#include<stdio.h>
int str_find_substring(char *str1, char *str2 )
    { 
	   int i=0;
      while(str1[i++] != '\0')
	    {
		 if(str1[i] ==str2[i])
		    {
		     return i;
		    }
		}
	  return -1;

    }
	
 int main()
    {
     char a[100],b[100];
	 printf("Enter the string1:");
     gets(a);
	 printf("Enter the string2:");
	 gets(b);
	 printf("String find function result is %d",str_find_substring(a,b));
    return 0;
    }



